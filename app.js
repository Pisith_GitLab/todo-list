// create express app
const express = require("express");
const app = express();
const cors = require("cors");
const connectDB = require('./datasource/db');
const todoRoutes = require('./routes/todoRoutes');

app.use(cors());
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

connectDB();

// define a simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to TODO application." });
})

// import routes

app.use('/api/todos', todoRoutes);

// listen for requests
app.listen(3000, () => {
  console.log("Server is listening on port 3000");
})