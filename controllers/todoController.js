// create {createTodo, getTodos} controllers and export to routes
const Todo = require('../models/todo');

const createTodo = async (req, res) => {
    try {
        const { title, description, status } = req.body;
        const todo = new Todo({ title, description, status });
        await todo.save();
        res.status(201).json({ success: true, message: 'Todo created successfully', data: todo });
      } catch (error) {
        res.status(500).json({ success: false, error: 'Failed to create todo' });
      }
}
const getTodos = async (req, res) => {
    try {
        const todos = await Todo.find();
        res.status(200).json({ success: true, data: todos });
      } catch (error) {
        res.status(500).json({ success: false, error: 'Failed to get todos' });
    }
}
// Controller for getting a Todo by ID
const getTodoById = async (req, res) => {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        return res.status(404).json({ error: 'Todo not found' });
      }
      res.status(200).json(todo);
    } catch (err) {
      res.status(500).json({ error: 'Internal server error' });
    }
  };
  
  // Controller for updating a Todo by ID
const updateTodoById = async (req, res) => {
    try {
      const todo = await Todo.findByIdAndUpdate(req.params.id, req.body, { new: true });
      if (!todo) {
        return res.status(404).json({ error: 'Todo not found' });
      }
      res.status(200).json(todo);
    } catch (err) {
      res.status(500).json({ error: 'Internal server error' });
    }
  };
  
  // Controller for deleting a Todo by ID
const deleteTodoById = async (req, res) => {
    try {
      const todo = await Todo.findByIdAndDelete(req.params.id);
      if (!todo) {
        return res.status(404).json({ error: 'Todo not found' });
      }
      res.status(200).json({ message: 'Todo deleted successfully' });
    } catch (err) {
      res.status(500).json({ error: 'Internal server error' });
    }
  };
module.exports = {
    createTodo,
    getTodos,
    getTodoById,
    updateTodoById,
    deleteTodoById
  }